import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver

System.setProperty("geb.build.reportsDir", "target/geb-reports")
System.setProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver.exe")
System.setProperty("geb.build.baseUrl", "https://www.nixsolutions.com/")
System.setProperty("autoqa.RemoteUrl", "http://10.8.49.98:4446/wd/hub")

hubUrl = new URL(System.getProperty("autoqa.RemoteUrl"))
driver = {
    def driver = new ChromeDriver()
    driver.manage().window().maximize()
    return driver
}

environments {
    'remote-chrome'{
        driver = {
            ChromeOptions options = new ChromeOptions();
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
            def driver = new RemoteWebDriver(hubUrl, capabilities)
            return driver
        }
    }
}