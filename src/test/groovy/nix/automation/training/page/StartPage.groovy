package nix.automation.training.page

import geb.Page

class StartPage extends Page {

    static at = {
        title == "NIX – Outsourcing Offshore Software Development Company"
    }

    static content = {
        blogLink (wait:true) { $("a", text: "Blog")}
    }

    def "User navigates to blog page"() {
        blogLink.click()
    }
}
