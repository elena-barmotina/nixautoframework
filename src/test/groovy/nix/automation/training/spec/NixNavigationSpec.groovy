package nix.automation.training.spec

import geb.spock.GebReportingSpec
import nix.automation.training.page.StartPage
import nix.automation.training.page.BlogPage

class NixNavigationSpec extends GebReportingSpec{

    def "Navigate to Blog page"() {

        when:
            to StartPage
            "User navigates to blog page"()
            println "Added text"

        then:
            at BlogPage
    }
}
